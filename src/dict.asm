%include "lib.inc"

%define jump_to_key 8
%define equal 1
%define dictionary_end 0x0 
section .text


global find_word
find_word:
    .find_word_loop:
        mov rdx, rsi
        add rsi, jump_to_key
        push rdi
        call string_equals
        pop rdi
        cmp rax, equal
        je .found_word
        cmp qword[rdx], dictionary_end
        je .nonfound_word
        mov rsi, [rdx]
        jmp .find_word_loop
    .nonfound_word:
        xor rax, rax
        ret
    .found_word:
        mov rax, rdx
	ret
