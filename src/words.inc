%include "colon.inc"

section .data
    colon "group", sixth_word
    db "P3233", 0

    colon "lab", fifth_word
    db "Second lab", 0

    colon "itmo", fourth_word
    db "university ITMO", 0

    colon "third", third_word
    db "third word", 0

    colon "username", second_word
    db "Aganin", 0

    colon "name", first_word
    db "Egor", 0
