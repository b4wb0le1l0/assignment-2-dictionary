section .text
global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_newline_stderr

%define SYSCALL_EXIT 60
%define SYSCALL_WR 1

%define STD_IN 0
%define STD_OUT 1

%define SYMB_SPACE ` `
%define SYMB_TAB `\t`
%define SYMB_NL `\n`
%define SYMB_MINUS `-`
%define SYMB_NUM_MIN '0'
%define SYMB_NUM_MAX '9'


section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .counter:
        cmp  byte [rdi+rax], 0
        je   .end
        inc  rax
        jmp  .counter
    .end:
        ret

global print_error
; Принимает указатель на нуль-терминированную строку, выводит ее в stderr
print_error:
    push rdi
    call string_length
    pop rsi
    mov rdi, stderr
    mov rdx, rax
    mov rax, write_syscall
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, STD_OUT
    mov rax, SYSCALL_WR
    syscall
    ret

print_newline_stderr:
    mov rdi, newline_char
    push rdi
    mov rsi, rsp
    mov rax, write_syscall
    mov rdi, stderr
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, SYMB_NL

; Принимает код символа и выводит его в stdout
print_char:
    push rdi       
    mov rsi, rsp
    mov rax, SYSCALL_WR
    mov rdi, STD_OUT
    mov rdx, 1        
    syscall
    pop rdi       
    ret 

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, rsp
    sub rsp, 32
    dec rdi
	mov byte[rdi], 0
    mov rcx, 10      ; Делитель - 10 для десятичной системы

    .reverse_loop:
        xor rdx, rdx
        div rcx           ; делим rax на 10, результат в rax, остаток в rdx

        add rdx, SYMB_NUM_MIN
        dec rdi
        mov [rdi], dl
	    test rax, rax

        jnz .reverse_loop

        call print_string
        add rsp, 32
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi      
    jns print_uint     ; если >=0 переходим в print_uint

    push rdi           ; сохраняем rdi на стеке
    mov rdi, SYMB_MINUS      
    call print_char    ; печатаем -
    pop rdi            ; восстанавливаем rdi со стека

    neg rdi            ; инвертируем число
    jmp print_uint     ; переходим в print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:

    .compare_strings:
        mov al, [rdi]    ; Загружаем символ из первой строки
        mov cl, [rsi]    ; Загружаем символ из второй строки
        cmp al, cl
        jne .not_equal

        test al, al      ; Проверяем, является ли символ нуль-терминатором
        jz .equal

        inc rdi         ; Увеличиваем указатель на первой строке
        inc rsi         ; Увеличиваем указатель на второй строке
        jmp .compare_strings

    .not_equal:
        xor rax, rax      ; Устанавливаем 0 в rax (строки не равны)
        ret

    .equal:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    xor rax, rax
    xor rdi, rdi
    mov rdx, STD_OUT
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12 
	push r13
	push r14
	mov r12, rdi 
	mov r13, rsi 
	dec r13 
	xor r14, r14 
.skip: 
	call read_char
	cmp rax, SYMB_SPACE
	je .skip
	cmp rax, SYMB_TAB
	je .skip
	cmp rax, SYMB_NL
	je .skip
.loop: 
	mov [r12+r14], rax
	test rax, rax
	jz .ret 
	cmp rax, SYMB_SPACE
	je .ret 
	cmp rax, SYMB_TAB
	je .ret 
	cmp rax, SYMB_NL
	je .ret 
	cmp r13, r14
	je .ret_err ; +`\0` 
	inc r14
	call read_char
	jmp .loop
.ret_err:
	xor r12, r12
	xor r14, r14
.ret:
	mov rax, r12 
	mov rdx, r14 
	pop r14
	pop r13
	pop r12
	ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor rsi, rsi
    mov r10, 10
    
    .loop:
        mov sil, byte[rdi + rcx]
        cmp sil, SYMB_NUM_MIN
        jl .end
        cmp sil, SYMB_NUM_MAX
        jg .end
        
        mul r10
        add rax, rsi
        sub rax, SYMB_NUM_MIN
        inc rcx
        
        jmp .loop
        
    .end:
        mov rdx, rcx
        ret   


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax             ; инвертируем
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx

    .loop:
        cmp byte[rdi + rcx], 0
        jz .end                     ; если байт '0' ухожим в .end
        mov al, byte[rdi + rcx]     
        mov byte[rsi + rcx], al     ; загружаем из памяти по указателю и записываем через al в буфер
        inc rcx                     
        cmp rdx, rcx                
        jl .zero_end                ; если длинна больше буфера, в .zero_end
        xor rax, rax                
        jmp .loop

    .end:
        cmp rcx, rdx
        je .good_end                ; Если rcx равно rdx, в .good_end
        mov byte[rsi + rcx], 0      ; добавляем нуль терминатор

    .good_end:                      ; возвращаем длинну строки
        mov rax, rcx
        ret

    .zero_end:                      ; обнуляем rax тк не поместилась в буфер.
        xor rax, rax
        ret
